# otus3
Сборка образа вручную папка в проекте spring-boot-rest 
docker build -t java:5.0 .
docker tag java:5.0 arty79/java:5.0
docker push arty79/java:5.0

Настройка миникуба
kubectl delete all --all
kubectl create namespace monitoring
kubectl config set-context --current --namespace=monitoring

Команда для установки prometheus
helm install prom prometheus-community/kube-prometheus-stack -f prometheus.yaml --atomic

Команда для установки nginx-controller
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install nginx stable/nginx-ingress -f nginx-ingress.yaml --atomic
Команда для добавления dashboard'а в grafan'у (имя dashboard'а "Service")

kubectl apply -f prometheus/grafana-product-dashboard.yaml 
kubectl apply -f prometheus/grafana.yaml 

Команда для проброса портов для grafana
kubectl port-forward service/prom-grafana 9000:80

Команда для проброса портов для prometeus
kubectl port-forward service/prom-kube-prometheus-stack-prometheus 9999:9090

Grafana credentials
user: admin pas: prom-operator

Команда запуска 

skaffold run 
helm install myapp-hello-chart ./hello-chart

при наличии плагина в системе, иначе

Запуск через helm

helm install myapp-hello-chart ./hello-chart

Для проверки в браузере

после вызова  kubectl get ing получить IP сервиса

и прописать его в etc/hosts  - например так 172.17.206.41  arch.homework

http://arch.homework/otusapp/actuator/health - эндпоинт для проверки работоспособности в браузере

http://arch.homework/otusapp/swagger-ui.html#/ - OpenApi(swagger)

скриншоты дашборда графаны в папке /screenshots




